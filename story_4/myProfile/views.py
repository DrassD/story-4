from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'myProfile.html')
	
def index2(request):
    return render(request, 'aboutMe.html')
	
def index3(request):
    return render(request, 'help.html')
	
def index4(request):
    return render(request, 'register.html')